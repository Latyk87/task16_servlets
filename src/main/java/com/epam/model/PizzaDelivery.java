package com.epam.model;


import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
/**
 * Class creates all pizzas and implements Servlets.
 *
 * @version 2.1
 * Created by Borys Latyk on 28/12/2019.
 * @since 28.11.2019
 */
@WebServlet("/sets/*")
public class PizzaDelivery extends HttpServlet {

    private static List<Pizza> pizzas = new ArrayList<>();
    private static List<Pizza> ordered = new ArrayList<>();

    @Override
    public void init() {

        Peperroni peperroni = new Peperroni(85, "500g", 35, "100g",
                "100g", "110g", "80g", true, 3);
        Cheese cheese = new Cheese(65, "400g", 30,
                "130g", "170g", "100g", "50g", true, 1);
        Veggie veggie = new Veggie(100, "600g", 40,
                "150g", "70g", "100g", "100g",
                true, true, "50g");

        pizzas.add(peperroni);
        pizzas.add(cheese);
        pizzas.add(veggie);
        Comparator<Pizza> pizzaComparator = ((o1, o2) -> o1.getPrice() - o2.getPrice());
        Collections.sort(pizzas, pizzaComparator);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setContentType("text/html");
        PrintWriter out = resp.getWriter();
        out.println("<html><head><html>");
        out.println("<p> <a href='sets'>REFRESH</a> </p>");
        out.println("<h2>Pizza Menu</h2>");
        for (Pizza p : pizzas) {
            out.println("<p>" + p + "</p>");
        }
        out.println("<h2>Your Order</h2>");
        for (Pizza p : ordered) {
            out.println("<p>" + p + "</p>");
        }
        out.println("<form action='sets' method='POST'>\n"
                + "  Input price from menu: <input type='number' name='price'>\n"
                + "  <button type='submit'>Add your Pizza to order</button>\n"
                + "</form>");
        out.println("<form>\n"
              + "  <p><b>DELETE ELEMENTS</p></b>\n"
              + "    <input type='button' onclick=remove(this.form.ordered) name='ok' value='Delete Order'"
              + "  </p>\n"
              + "</form>");

        out.println("<script type='number/javascript'>\n"
              + "  function remove(ordered)  { fetch('sets/' + ordered, {method: 'DELETE')); }\n "
              + "</script>");

        out.println("</body></html>");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String s = req.getParameter("price");
        int price = Integer.parseInt(s);
        for (Pizza p1 : pizzas) {
            if (p1.getPrice() == price) {
                ordered.add(p1);
            }
        }
        doGet(req, resp);
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws IOException {
       ordered.clear();


    }
}
