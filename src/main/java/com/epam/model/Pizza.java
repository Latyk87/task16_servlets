package com.epam.model;
/**
 * Interface for inheritance.
 *
 * @version 2.1
 * Created by Borys Latyk on 28/12/2019.
 * @since 28.11.2019
 */
public  interface Pizza {
     int getPrice();
}
