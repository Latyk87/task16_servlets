package com.epam.model;
/**
 * Class describes Pepperoni Pizzas.
 *
 * @version 2.1
 * Created by Borys Latyk on 28/12/2019.
 * @since 28.11.2019
 */
public class Peperroni implements Pizza {
    private int price;
    private String dough;
    private int doughsize;
    private String tomatoes;
    private String mozzellas;
    private String sausage;
    private String oliveoil;
    private boolean burnt;
    private int basil;

    public Peperroni(int price, String dough, int doughsize, String tomatoes, String mozzellas, String sausage, String oliveoil, boolean burnt, int basil) {
        this.price = price;
        this.dough = dough;
        this.doughsize = doughsize;
        this.tomatoes = tomatoes;
        this.mozzellas = mozzellas;
        this.sausage = sausage;
        this.oliveoil = oliveoil;
        this.burnt = burnt;
        this.basil = basil;
    }

    public int getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return "Peperroni{" +
                "price=" + price +
                ", dough='" + dough + '\'' +
                ", doughsize=" + doughsize +
                ", tomatoes='" + tomatoes + '\'' +
                ", mozzellas='" + mozzellas + '\'' +
                ", sausage='" + sausage + '\'' +
                ", oliveoil='" + oliveoil + '\'' +
                ", burnt=" + burnt +
                ", basil=" + basil +
                '}';
    }
}
