package com.epam.model;
/**
 * Class describes Veggie Pizzas.
 *
 * @version 2.1
 * Created by Borys Latyk on 28/12/2019.
 * @since 28.11.2019
 */
public class Veggie implements Pizza {
    private int price;
    private String dough;
    private int doughsize;
    private String tomatoes;
    private String tofuCheese;
    private String arugula;
    private String mushrooms;
    private boolean oliveoil;
    private boolean sabers;
    private String onion;

    public Veggie(int price, String dough, int doughsize, String tomatoes,
                  String tofuCheese, String arugula,
                  String mushrooms, boolean oliveoil, boolean sabers, String onion) {
        this.price = price;
        this.dough = dough;
        this.doughsize = doughsize;
        this.tomatoes = tomatoes;
        this.tofuCheese = tofuCheese;
        this.arugula = arugula;
        this.mushrooms = mushrooms;
        this.oliveoil = oliveoil;
        this.sabers = sabers;
        this.onion = onion;
    }

    public int getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return "Veggie{" +
                "price=" + price +
                ", dough='" + dough + '\'' +
                ", doughsize=" + doughsize +
                ", tomatoes='" + tomatoes + '\'' +
                ", tofuCheese='" + tofuCheese + '\'' +
                ", arugula='" + arugula + '\'' +
                ", mushrooms='" + mushrooms + '\'' +
                ", oliveoil=" + oliveoil +
                ", sabers=" + sabers +
                ", onion='" + onion + '\'' +
                '}';
    }
}
